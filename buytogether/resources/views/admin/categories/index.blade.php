@include('admin.layouts.header')
@include('admin.layouts.menu')

    <section id="content">
        <div class="container">
            <div class="block-header">
                <h2>Categories</h2>
            </div>

            <div class="card">
                <div class="card-header">
                    <a href="{{url('/categories/create')}}" class="btn bgm-gray waves-effect">Create</a>
                </div>

                <table id="data-table-command" class="table table-striped table-vmiddle">
                    <thead>
                    <tr>
                        <th data-column-id="id" data-type="numeric">ID</th>
                        <th data-column-id="nom">Name</th> 
                        <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category) 
                        <tr>
                            <td>{!! $category->id !!}</td>
                            <td>{!! $category->name_ca !!}</td> 
                            <!--<td>
                                <button type="button" class="btn btn-icon command-edit waves-effect waves-circle" data-row-id="{!! $category->id !!}"><span class="zmdi zmdi-edit"></span></button>
                            </td>-->
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>

@include('admin.layouts.footer')

<!-- Data Table -->
<script type="text/javascript">
    $(document).ready(function(){
        //Command Buttons
        $("#data-table-command").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-sort-amount-desc',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-sort-amount-asc'
            },
            formatters: {
                "commands": function(column, row) {
                    return "<a href=\"window.location.href='{!! URL::to('/categories') !!}'\" class=\"btn btn-icon command-edit waves-effect waves-circle\" ><span class=\"zmdi zmdi-edit\"></span></a> " +
                            "<button type=\"button\" class=\"btn btn-icon command-delete waves-effect waves-circle\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-delete\"></span></button>";
                }
            }
            /*formatters: {
                "commands": function(column, row) {
                    return "<button type=\"button\" onclick=\"window.location.href='{!! URL::to('/categories') !!}'\" class=\"btn btn-icon command-edit waves-effect waves-circle\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-edit\"></span></button> " +
                            "<button type=\"button\" class=\"btn btn-icon command-delete waves-effect waves-circle\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-delete\"></span></button>";
                }
            }*/
        });
    });
</script>