<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name_ca', 'name_es', 'name_en', 'created_at', 'updated_at'
    ];

    /*public function productes()
    {
        return $this->hasMany('App\Products' , 'foreign_key' , "local_key");
        return $this->hasMany('App\Products' , 'category_id' , "id");
    }

       public function catalegs()
    {
        return $this->hasMany('App\Products' , 'foreign_key' , "local_key");
        return $this->hasMany('App\Products' , 'category_id' , "id");
    }*/

}
